#! /usr/bin/env python3

from skimage import io
from skimage.filters import gaussian
from skimage.segmentation import active_contour
import matplotlib.pyplot as plt
import numpy as np

img = io.imread("nematode.jpeg")

s = np.linspace(0, 2 * np.pi, 300)
r = 75 + 70 * np.sin(s)
c = 150 + 80 * np.cos(s)
init = np.array([r, c]).T

preproc_img = img
preproc_img = gaussian(preproc_img, 2)

snake = active_contour(preproc_img,
                       init, alpha=0.02, beta=0.00001, gamma=0.0016,
                       boundary_condition="periodic")

fig, ax = plt.subplots(figsize=(7, 7))
ax.imshow(img, cmap=plt.cm.gray)
ax.plot(init[:, 1], init[:, 0], '--r', lw=3)
ax.plot(snake[:, 1], snake[:, 0], '-b', lw=3)
ax.set_xticks([]), ax.set_yticks([])
ax.axis([0, img.shape[1], img.shape[0], 0])

plt.show()
